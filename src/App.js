import { useState } from "react";

const initialFriends = [
  {
    id: 118836,
    name: "Liza",
    image: "https://i.pravatar.cc/48?u=339",
    balance: -7,
  },
  {
    id: 933372,
    name: "Sandra",
    image: "https://i.pravatar.cc/48?u=355",
    balance: 20,
  },
  {
    id: 499476,
    name: "Miguel",
    image: "https://i.pravatar.cc/48?u=499476",
    balance: 0,
  },
];

function App() {
  const [friends, setFriends] = useState(initialFriends);
  const [selectedFriend, setSelectedFriend] = useState(null);
  const [openAddFriend, setOpenAddFriend] = useState(false);

  function handleAddFriend(friend) {
    setFriends((friends) => [...friends, friend]);
    setOpenAddFriend(false);
  }

  // use existing useState, create a new array,
  // if friend.id to be checked === id, then
  // spread its fields and change the "balance" value,
  // and w else do not change, just leave the friend as it is
  function handleSplitBill(value) {
    setFriends((friends) =>
      friends.map((friend) =>
        friend.id === selectedFriend?.id
          ? { ...friend, balance: friend.balance + value }
          : friend
      )
    );
    setSelectedFriend(null);
  }

  function handleSelectedFriend(friend) {
    setSelectedFriend((selected) => (selected?.id === friend.id ? {} : friend));
    setOpenAddFriend(false);
  }

  return (
    <div className="app">
      <div className="sidebar">
        <FriendList
          friends={friends}
          selectedFriend={selectedFriend}
          onSelectFriend={handleSelectedFriend}
        />
        {openAddFriend && <AddFriendForm onAddFriend={handleAddFriend} />}
        <Button onButtonClick={() => setOpenAddFriend((open) => !open)}>
          {openAddFriend ? "Close" : "Add a friend"}
        </Button>
      </div>

      <div>
        {selectedFriend && (
          <SplitBill
            selectedFriend={selectedFriend}
            onSelectFriend={setSelectedFriend}
            onSplitBill={handleSplitBill}
            onOpenAddFriend={setOpenAddFriend}
            key={selectedFriend.id}
          />
        )}
      </div>
    </div>
  );
}

function FriendList({ friends, onSelectFriend, selectedFriend }) {
  return (
    <ul>
      {friends.map((friend) => (
        <Friend
          friend={friend}
          key={friend.id}
          selectedFriend={selectedFriend}
          onSelectFriend={onSelectFriend}
        />
      ))}
    </ul>
  );
}

function Friend({ friend, selectedFriend, onSelectFriend }) {
  const selected = friend.id === selectedFriend?.id;

  return (
    <li className={selected ? "selected" : ""}>
      <img src={friend.image} alt={friend.name} />
      <h3>{friend.name}</h3>

      {friend.balance === 0 && (
        <p className="">You and {friend.name} are even</p>
      )}
      {friend.balance < 0 && (
        <p className="red">
          You owe {friend.name} {Math.abs(friend.balance)} EUR
        </p>
      )}
      {friend.balance > 0 && (
        <p className="green">
          {friend.name} owes you {Math.abs(friend.balance)} EUR
        </p>
      )}

      <Button onButtonClick={() => onSelectFriend(friend)}>
        {selected ? "Close" : "Select"}
      </Button>
    </li>
  );
}

function SplitBill({ selectedFriend, onSplitBill }) {
  const [bill, setBill] = useState(0);
  const [myExpense, setMyExpense] = useState(0);
  const [whoPays, setWhoPays] = useState("Me");
  const friendsExpence = bill - myExpense;

  function handleMyBill(e) {
    setMyExpense(
      Number(e.target.value) <= bill ? Number(e.target.value) : bill
    );
  }

  function SplitBill(event) {
    event.preventDefault();

    onSplitBill(whoPays === "Me" ? friendsExpence : -myExpense);
  }

  return (
    <form className="form-split-bill" onSubmit={SplitBill}>
      <h2>Split a bill with {selectedFriend.name}</h2>
      <label>Bill value:</label>
      <input
        type="number"
        value={bill}
        onChange={(e) => setBill(Number(e.target.value))}
      />
      <label>Your expense:</label>
      <input type="number" value={myExpense} onChange={handleMyBill} />
      <label>{selectedFriend.name}'s expense</label>
      <input type="number" disabled={true} value={friendsExpence} />
      <label>Who is paying the bill?</label>
      <select value={whoPays} onChange={(e) => setWhoPays(e.target.value)}>
        <option value="Me">Me</option>
        {selectedFriend.id > 0 && (
          <option value={selectedFriend.name}>{selectedFriend.name}</option>
        )}
      </select>
      <Button>Split bill</Button>
    </form>
  );
}

function AddFriendForm({ onAddFriend }) {
  const [friendName, setFriendName] = useState("");
  const [imageUrl, setImageUrl] = useState("https://i.pravatar.cc/48");

  function handleChange(event) {
    event.preventDefault();
    if (!imageUrl || !friendName || friendName.trim() === "") return;

    const url = imageUrl + "?u=" + crypto.randomUUID();

    const newFriend = {
      id: Date.now(),
      name: friendName,
      image: url,
      balance: 0,
    };

    onAddFriend(newFriend);
    setFriendName("");
    setImageUrl("https://i.pravatar.cc/48");
  }

  return (
    <form className="form-add-friend" onSubmit={handleChange}>
      <label>Friend name: </label>

      <input
        type="text"
        value={friendName}
        onChange={(e) => setFriendName(e.target.value)}
      />

      <label>Image url:</label>
      <input
        type="text"
        value={imageUrl}
        onChange={(e) => setImageUrl(e.target.value)}
      />

      <Button>Add friend</Button>
    </form>
  );
}

function Button({ children, onButtonClick }) {
  return (
    <button className="button" onClick={onButtonClick}>
      {children}
    </button>
  );
}
export default App;
