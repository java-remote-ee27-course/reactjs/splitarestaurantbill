# Split The Restaurant Bill With A Friend

App to split the restaurant bill with a friend - calculates the balance after the bill is paid by me vs. friend.

## Features

- NEW: reset Split bill form data when changing a friend (added key to SplitBill element for the purpose)
- Open-close "Add a friend" form
- Add friends to the friends list
- Add your bill value & your expense => friend's expense is auto-calculated
- Select who pays, the result who owes you or to whom you owe is calculated based by the previous values
- Conditional styling based on selected friend
- Select / unselect a friend to split a bill with, if Select button is pressed
- Control the auto close of "Add friend" and "Bill" forms
- Control my maximum expense.

## Created by

- Author of JSX: Katlin.
- Styling based on: https://www.udemy.com/course/the-ultimate-react-course/learn/lecture/37350678

![bill demo](./public/bill1.png)

![bill demo](./public/bill2.png)

![bill demo](./public/bill3.png)

![bill demo](./public/bill4.png)
